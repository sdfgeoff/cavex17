#ifndef POSITION_H
#define POSITION_H
#include <inttypes.h>

typedef struct position_t {
    uint8_t x;
    uint8_t y;
} Position;


Position POS(uint8_t, uint8_t);
uint16_t dist(Position p1, Position p2);

#endif
