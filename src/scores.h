#ifndef SCORES_H
#define SCORES_H
#include <inttypes.h>

#define NUM_SCORES 13
#define SCORE_STR_LEN 40

typedef struct score_t {
    uint16_t startTime; //These don't need to contain the entire number because
    uint16_t endTime;   //Subtraction cancels the difference out (I hope)
    uint8_t percentExplored;
    uint16_t numberSteps;
    uint16_t mapID;
} Score;

Score* getScore(uint8_t age);
void incrementScoreAges(void);
void formatScoreString(Score* score, char* outString);
void getScoreString(uint8_t age, char* outString);

#endif
