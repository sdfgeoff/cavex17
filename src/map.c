#include <stdio.h>
#include <stdlib.h>
#include "map.h"

MapNode OUTSIDE_BOUND_NODE = {
    WALL,
    1
};

/* Returns a pointer to the node at a given point */
MapNode* getNode(Map* rawMap, Position pos){
    if((pos.x >= X_SIZE) || (pos.x < 0) || (pos.y >= Y_SIZE) || (pos.y < 0)) {
                return &OUTSIDE_BOUND_NODE;
        }
    return &rawMap->nodeArray[pos.x][pos.y];
}

/* Fills the map with binary noise empty/wall */
void initialNoise(Map* rawMap){
    MapNode* currentNode;
    for (int x=0; x<X_SIZE; x++){
        for (int y=0; y<Y_SIZE; y++){
            currentNode = getNode(rawMap, POS(x, y));
            currentNode->seen = 0;
            if (rand()%2 == 0){
                currentNode->terrain = EMPTY;
            } else {
                currentNode->terrain = WALL;
            }
        }
    }

}

/* Joins rooms by removing thin walls */
void joinRooms(Map* rawMap){
    for (int x=0; x<X_SIZE; x++){
        for (int y=0; y<Y_SIZE; y++){
	    if (getNode(rawMap, POS(x+1, y))->terrain == getNode(rawMap, POS(x-1, y))->terrain && getNode(rawMap, POS(x, y-1))->terrain == getNode(rawMap, POS(x,y+1))->terrain){
		    getNode(rawMap, POS(x, y))->terrain = EMPTY;
	    }
	}
    }
}

/* Gets the number of neigbours around point (x,y).
 * It will check all 8 neighbours */
static int getNumNeighbours(Map* rawMap, Position pos){
    int sum = 0;
    for (int x=-1; x<=1; x++){
        for (int y=-1; y<=1; y++){
	    if ((x != 0) || (y != 0)){
		if (getNode(rawMap, POS(x+pos.x,y+pos.y))->terrain == WALL){
                    sum += 1;
                }
	    }
	}
    }
    return sum;
}

static void wander_exit(Map* rawMap) {
    int direction = rand() & 3; //Extract the last two bits ie a number between 1 and 4
    Position next = {0,0};
    if (direction == 0){
        next.x = 1; next.y = 0;
    } else if (direction == 1){
        next.x = -1; next.y = 0;
    } else if (direction == 2){
        next.x = 0; next.y = 1;
    } else if (direction == 3){
        next.x = 0; next.y = -1;
    }
    while (getNode(rawMap, POS(rawMap->endPos.x+next.x, rawMap->endPos.y+next.y))->terrain == EMPTY){
        rawMap->endPos.x += next.x;
        rawMap->endPos.y += next.y;
    }
}

void placeEntities(Map* rawMap){
    Position startPos = {0, 0};
    // Place Start Position
    startPos.y = rand() % Y_SIZE;
    while (startPos.x < X_SIZE-1) {
    if (getNode(rawMap, startPos)->terrain == EMPTY && getNumNeighbours(rawMap, startPos) <= 3){
        break;
            }
            startPos.x += 1;
    }
    getNode(rawMap, startPos)->terrain = START;

    rawMap->startPos = startPos;
    rawMap->endPos = startPos;
    for (int i=0; i<1000; i++){
        wander_exit(rawMap);
        if (i > 500 && dist(rawMap->endPos, rawMap->startPos) > 7);
    }
    getNode(rawMap, rawMap->endPos)->terrain = STAIRS;
}

void generateMap(Map* rawMap, uint16_t mapId){
    srand(mapId << 8);// avoid map duplication due to rand() algorithm
                         // rand(seed) called twice is equal to rand(seed+1)
    initialNoise(rawMap);
    joinRooms(rawMap);
    joinRooms(rawMap);
    placeEntities(rawMap);
}

uint16_t getPercentSeen(Map* rawMap){
    uint16_t sum = 0;
    uint16_t total = 0;
    MapNode* currentNode;
    for (int x=0; x<X_SIZE; x++){
        for (int y=0; y<Y_SIZE; y++){
            currentNode = getNode(rawMap, POS(x, y));
	    if (currentNode->seen != 0){
		sum += 1;
	    }
	    total += 1;
	}
    }
    return 100 * sum / total;
}


void printMap(Map* rawMap, uint8_t border){
    MapNode* currentNode;
    for (int y=-border; y<Y_SIZE+border; y++){
        for (int x=-border; x<X_SIZE+border; x++){
            currentNode = getNode(rawMap, POS(x, y));
            if (currentNode->terrain == EMPTY){
                printf("  ");
            } else if (currentNode->terrain == WALL) {
                printf("[]");
            } else if (currentNode->terrain == START) {
                printf(":)");
            }
        }
        printf("\n");
    }
}

