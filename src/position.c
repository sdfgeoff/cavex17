#include "position.h"
#include <stdlib.h>

/* Constructor for making positions really easily */
Position POS(uint8_t x, uint8_t y){
    Position p = {x, y};
    return p;
}

uint16_t dist(Position p1, Position p2){
    return abs(p1.x - p2.x) + abs(p1.y + p2.y);
}
