#include "input.h"
#include <ncurses.h>

Action getActiveInput(void){
    int ch = getch();
    switch (ch){
        case 'w':
        case 'W':
        case 'k':
        case 'K':
            return NORTH;
            break;
        case 's':
        case 'S':
        case 'j':
        case 'J':
            return SOUTH;
            break;
        case 'a':
        case 'A':
        case 'h':
        case 'H':
            return WEST;
            break;
        case 'd':
        case 'D':
        case 'L':
        case 'l':
            return EAST;
            break;
        case 'q':
            return QUIT;
            break;

    }
    return NONE;
}
