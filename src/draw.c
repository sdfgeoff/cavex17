#include <ncurses.h>
#include "draw.h"
#include "graphics.h"
#include "scores.h"
#include "player.h"
#include "map.h"

#include <stdlib.h>


WINDOW* game_display;
WINDOW* logo;
WINDOW* sidebar;

void initDraw(void){
    game_display = newwin(GAME_HEIGHT, GAME_WIDTH, GAME_Y, GAME_X);
    logo = newwin(LOGO_HEIGHT, LOGO_WIDTH, LOGO_Y, LOGO_X);
    sidebar = newwin(SIDEBAR_HEIGHT, SIDEBAR_WIDTH, SIDEBAR_Y, SIDEBAR_X);

    refresh();
}

void drawBox(WINDOW* win){
    wborder(win, '|', '|', '-', '-', '+', '+', '+', '+');
}

void drawLogo(WINDOW* win){
    for (int i=0; i<LOGO_HEIGHT; i++){
        mvwprintw(win, i, 0, LOGO[i]);
    }
}

void drawScores(WINDOW* win){
    char scoreStr[SCORE_STR_LEN];

    for (int i=0; i<NUM_SCORES; i++){
        getScoreString(i, scoreStr);
        if (i < 1){
            mvwprintw(win, SIDEBAR_HEIGHT - 2 - i, 1, scoreStr);
        } else if (i == 1){
            mvwprintw(win, SIDEBAR_HEIGHT - 2 - i, 1, SIDEBAR_SEPERATOR);
            mvwprintw(win, SIDEBAR_HEIGHT - 3 - i, 1, scoreStr);
        } else {
            mvwprintw(win, SIDEBAR_HEIGHT - 3 - i, 1, scoreStr);
        }

    }

}

void drawSidebarHeader(WINDOW* win){
    for (int i=0; i<SIDEBAR_HEADER_HEIGHT; i++){
        mvwprintw(win, i+1, 1, SIDEBAR_HEADER[i]);
    }
}

void drawMap(WINDOW* win, Map* map){
    uint8_t playerX = getPlayerPosition().x;
    uint8_t playerY = getPlayerPosition().y;

    MapNode* currentNode;
    for (int y=-MAP_BORDER; y<Y_SIZE+MAP_BORDER; y++){
        for (int x=-MAP_BORDER; x<X_SIZE+MAP_BORDER; x++){
            currentNode = getNode(map, POS(x, y));
            if (abs(x - playerX) < VISIBLE_DISTANCE && abs(y - playerY) < VISIBLE_DISTANCE){
                if (currentNode->terrain == EMPTY || currentNode->terrain == START){
                    mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, EMPTY_GRAPHICS);
                } else if (currentNode->terrain == WALL) {
                    mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, WALL_GRAPHICS);
                } else if (currentNode->terrain == STAIRS) {
                    mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, EXIT_GRAPHICS);
                }
            } else {
                if (currentNode->seen == 1){
                    //Not in players view but has been seen
                    if (currentNode->terrain == WALL){
                        mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, SEEN_GRAPHICS);
                    }  else if (currentNode->terrain == STAIRS) {
                        mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, EXIT_GRAPHICS);
                    } else {
                        mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, EMPTY_GRAPHICS);
                    }
                } else {
                    //Not been seen yet
                    mvwprintw(win, y+MAP_OFFSET_Y, x*2+MAP_OFFSET_X, UNSEEN_GRAPHICS);
                }
            }
        }
    }
    mvwprintw(win, playerY+MAP_OFFSET_Y, playerX*2+MAP_OFFSET_X, ":)");
}

void reDraw(Map* map){
    drawBox(game_display);
    drawLogo(logo);
    drawBox(sidebar);

    drawSidebarHeader(sidebar);
    drawScores(sidebar);
    drawMap(game_display, map);

    wrefresh(game_display);
    wrefresh(logo);
    wrefresh(sidebar);

}


