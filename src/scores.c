#include "scores.h"
#include <stdio.h>
#include <time.h>

Score ScoreList[NUM_SCORES];
uint8_t ageOffset = 0;
uint8_t scoresSoFar = 0;

// 0 is the current score, 1 is the next newest, NUM_SCORES is the oldest
// Getting a newScore increments everything down the list
// It's a bit like hotel infinity, you can make more space by shuffling
// everything down by one

Score* getScore(uint8_t age){
    return &ScoreList[age];
}

void incrementScoreAges(void){
    for (int i=NUM_SCORES-1; i>0; i--){
        Score* newerScore = getScore(i-1);
        Score* olderScore = getScore(i);
        olderScore->startTime = newerScore->startTime;
        olderScore->endTime = newerScore->endTime;
        olderScore->percentExplored = newerScore->percentExplored;
        olderScore->numberSteps = newerScore->numberSteps;
        olderScore->mapID = newerScore->mapID;
    }

    Score* newestScore = getScore(0);
    newestScore->startTime = (uint16_t)time(NULL);;
    newestScore->endTime = (uint16_t)time(NULL);;
    newestScore->percentExplored = 0;
    newestScore->numberSteps = 0;
    newestScore->mapID = 0;

    scoresSoFar += 1;
}

void formatScoreString(Score* score, char* outString){
    uint16_t playTime = score->endTime - score->startTime;
    sprintf(outString, "#%04X% 4i sec% 4i%%%% seen% 4i steps", score->mapID, playTime, score->percentExplored, score->numberSteps);
}

void getScoreString(uint8_t age, char* outString){
    if (age < scoresSoFar){
        formatScoreString(getScore(age), outString);
    } else {
        sprintf(outString, " ");
    }
}
