#ifndef INPUT_H
#define INPUT_H

typedef enum action_e {
    NONE,
    NORTH,
    SOUTH,
    EAST,
    WEST,
    QUIT,
} Action;

Action getActiveInput(void);

#endif
