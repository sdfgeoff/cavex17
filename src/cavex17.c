#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <ncurses.h>

#include "map.h"
#include "scores.h"
#include "draw.h"
#include "input.h"
#include "player.h"


void newGame(void){

}

int main(int argc, char **argv){
    Map currentMap;

    // GET MAP ID FROM COMMAND LINE
    uint16_t mapId = 0;
    if (argc > 1){
        printf("Using input map");
        mapId = strtol(argv[1], NULL, 0);
    } else {
        mapId = (uint16_t)time(NULL);
    }

    //INIT NCURSES TO HANDLE DISPLAY AND INPUT
    initscr();
    refresh();
    noecho();
    timeout(500);
    curs_set(0);
    initDraw();

    //GET THE FIRST SCORE
    Score* currentScore = NULL;
    incrementScoreAges();
    currentScore = getScore(0);
    currentScore->mapID = mapId;

    //GENERATE THE FIRST MAP
    generateMap(&currentMap, mapId);
    setPlayerPos(currentMap.startPos);

    //GAME LOOP
    Action inp = getActiveInput();
    while(inp != QUIT){
        movePlayer(&currentMap, inp);
        reDraw(&currentMap);
        currentScore->endTime = (uint16_t)time(NULL);
        currentScore->percentExplored = getPercentSeen(&currentMap);

        if (getPlayerPosition().x == currentMap.endPos.x && getPlayerPosition().y == currentMap.endPos.y){
            incrementScoreAges();
            currentScore = getScore(0);
            mapId = rand();
            currentScore->mapID = mapId;
            generateMap(&currentMap, mapId);
            setPlayerPos(currentMap.startPos);
        } else {
            inp = getActiveInput();
        }
    }

    clrtoeol();
	refresh();
	endwin();
    return 0;
}


