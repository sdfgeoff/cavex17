#ifndef MAP_H
#define MAP_H

#include <inttypes.h>
#include "position.h"

#define X_SIZE 14
#define Y_SIZE 14

// TYPES
typedef enum terrain_e {
    EMPTY,
    WALL,
    START,
    STAIRS
} TerrainType;

typedef struct map_node_t {
    TerrainType terrain;
    uint8_t seen;
} MapNode;

typedef struct map_t {
    MapNode nodeArray[X_SIZE][Y_SIZE];
    uint16_t mapId;
    Position startPos;
    Position endPos;
} Map;

//Functions
MapNode* getNode(Map*, Position);
void generateMap(Map*, uint16_t);
void printMap(Map*, uint8_t);
uint16_t getPercentSeen(Map*);


#endif
