// A Mock up:

/*
    ______                _  __   ______ __ +---------------------------------+
   / ____/___ __   _____ | |/ /  /_  / // / | CONTROLS:                       |
  / /   / __ `/ | / / _ \|   /    / / // /_ |                                 |
 / /___/ /_/ /| |/ /  __/   |    / /__  __/ | Move:        WSAD, Arrows, JKL; |
 \____/\__,_/ |___/\___/_/|_|   /_/  /_/    | Options:                      e |
+------------------------------------------+| Quit:              ESC, ^C or q |
|   [][][][][][][][][][][][][][][][][][]   ||---------------------------------|
|   [][][][][][][][][][][][][][][][][][]   || COMPLETED MAPS:                 |
|   [][][]    [][][]  []  [][][][]  [][]   ||                                 |
|   [][]                            [][]   ||                                 |
|   [][]    []  [][]  []  [][][]  [][][]   ||                                 |
|   [][]    []  [][]  []  [][]    [][][]   ||                                 |
|   [][][]      [][][]  [][]      [][][]   ||                                 |
|   [][]                          [][][]   ||                                 |
|   [][]                      [][]  [][]   ||                                 |
|   [][]  []      []      []  []  [][][]   ||                                 |
|   [][][][][][]  []      []    []  [][]   ||                                 |
|   [][][]  [][]      [][]      []  [][]   ||                                 |
|   [][]      []  []            []  [][]   ||                                 |
|   [][]          []  []  []      [][][]   ||                                 |
|   [][]              []  []        [][]   ||                                 |
|   [][]      []  [][]      [][][][][][]   ||                                 |
|   [][][][][][][][][][][][][][][][][][]   ||---------------------------------|
|   [][][][][][][][][][][][][][][][][][]   ||#0000   0 sec   0% seen   0 steps|
+------------------------------------------++---------------------------------+
*/

#define TOTAL_HEIGHT 24
#define TOTAL_WIDTH 80

#define SIDEBAR_WIDTH 36
#define SIDEBAR_HEIGHT TOTAL_HEIGHT

#define LOGO_X 0
#define LOGO_Y 0
#define LOGO_HEIGHT 5
#define LOGO_WIDTH GAME_WIDTH

#define GAME_WIDTH (TOTAL_WIDTH - SIDEBAR_WIDTH)
#define GAME_HEIGHT (TOTAL_HEIGHT - LOGO_HEIGHT - 1)
#define GAME_X 0
#define GAME_Y (LOGO_HEIGHT) + 1

#define SIDEBAR_X GAME_WIDTH
#define SIDEBAR_Y 0

const char SIDEBAR_SEPERATOR[] = {
    "----------------------------------",
};


const char* LOGO[] = {
   "    ______                _  __   _________ ",
   "   / ____/___ __   _____ | |/ /  /_  /__  / ",
  "  / /   / __ `/ | / / _ \\|   /    / / ,',' ",
   " / /___/ /_/ /| |/ /  __/   |    / /,',' ",
" \\____/\\__,_/ |___/\\___/_/|_|   /_//_/    ",
};

const int SIDEBAR_HEADER_HEIGHT = 8;
const char* SIDEBAR_HEADER[] = {
" CONTROLS:                        ",
"                                  ",
" Move:                 WSAD, HJKL ",
" Options:                       e ",
" Quit:                    ^C or q ",
"----------------------------------",
" COMPLETED MAPS:                  ",
"''''''''''''''''''                ",
};

const char* WALL_GRAPHICS = "[]";
const char* EMPTY_GRAPHICS = "  ";
const char* SEEN_GRAPHICS = "::";
const char* PLAYER_GRAPHICS = ":)";
const char* UNSEEN_GRAPHICS = "??";
const char* EXIT_GRAPHICS = "><";

#define MAP_OFFSET_X 8
#define MAP_OFFSET_Y 2

#define MAP_BORDER 1
