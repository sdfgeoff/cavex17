#ifndef PLAYER_H
#define PLAYER_H
#include "input.h"
#include "position.h"
#include "map.h"

#define VISIBLE_DISTANCE 4

void movePlayer(Map* rawMap, Action inp);
Position getPlayerPosition(void);
void setPlayerPos(Position pos);

#endif
