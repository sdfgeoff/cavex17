#include "player.h"
#include "input.h"
#include "scores.h"

Position playerPos;

Position handleInput(Action inp){
	int8_t deltaX = 0;
	int8_t deltaY = 0;
	switch (inp){
		case NORTH:
			deltaY -= 1;
			break;
		case SOUTH:
			deltaY += 1;
			break;
		case EAST:
			deltaX += 1;
			break;
		case WEST:
			deltaX -= 1;
			break;
		case NONE:
		case QUIT:
			break;
	};
	return POS(deltaX, deltaY);
}

void updateSeen(Map* rawMap, Position plpos){
    MapNode* currentNode;
    for (int x=-VISIBLE_DISTANCE+1; x<VISIBLE_DISTANCE; x++){
        for (int y=-VISIBLE_DISTANCE+1; y<VISIBLE_DISTANCE; y++){
            currentNode = getNode(rawMap, POS(x+plpos.x, y+plpos.y));
            currentNode->seen = 1;
        }
    }
}

void movePlayer(Map* rawMap, Action inp){
	Position delta = handleInput(inp);
	Position newPos = playerPos;
	newPos.x += delta.x;
	newPos.y += delta.y;

	MapNode* targetPos = getNode(rawMap, newPos);
	if (targetPos->terrain != WALL && inp != NONE){
		getScore(0)->numberSteps += 1;
		playerPos = newPos;
	}
    updateSeen(rawMap, playerPos);
}

Position getPlayerPosition(void){
	return playerPos;
}

void setPlayerPos(Position pos){
	playerPos = pos;
}
