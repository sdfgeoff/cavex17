CC = gcc
CFLAGS = -Wall -Werror -lncurses -lm

SRC_FOLDER = ./src/
BIN_FOLDER = ./bin/

all : cavex17

cavex17 : $(SRC_FOLDER)cavex17.c map.o draw.o input.o player.o
	$(CC) $(CFLAGS) $(SRC_FOLDER)cavex17.c map.o position.o scores.o draw.o input.o player.o -o $(BIN_FOLDER)cavex17

map.o : $(SRC_FOLDER)map.c $(SRC_FOLDER)map.h position.o
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)map.c

position.o : $(SRC_FOLDER)position.c $(SRC_FOLDER)position.h
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)position.c

scores.o : $(SRC_FOLDER)scores.c $(SRC_FOLDER)scores.h
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)scores.c

draw.o : $(SRC_FOLDER)draw.c $(SRC_FOLDER)draw.h $(SRC_FOLDER)graphics.h scores.o player.o map.o
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)draw.c

input.o : $(SRC_FOLDER)input.c $(SRC_FOLDER)input.h
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)input.c

player.o : $(SRC_FOLDER)player.c $(SRC_FOLDER)player.h scores.o
	$(CC) $(CFLAGS) -c $(SRC_FOLDER)player.c

run : cavex17
	$(BIN_FOLDER)cavex17
